#!/usr/bin/env python
import pygame,os,sys
from pygame.locals import *
import random

GRID_WIDTH = 40
GRID_HEIGHT = 20
PIECE_SIZE = 20
LEFT = 0
RIGHT = 1
UP = 2
DOWN = 3

class Food:
    position = (0, 0)
    def __init__(self):
        self.position = (random.randint(0,GRID_WIDTH-1), random.randint(0,GRID_HEIGHT-1))
    def draw(self, surface):
        pygame.draw.ellipse(screen, (255,255,255), pygame.Rect(self.position[0]*PIECE_SIZE, self.position[1]*PIECE_SIZE, PIECE_SIZE, PIECE_SIZE))

class SnakeSegment:
    position = (0, 0)
    def __init__(self, p):
        self.position = p
    def draw(self, surface):
        pygame.draw.rect(screen, (0,255,0), pygame.Rect(self.position[0]*PIECE_SIZE, self.position[1]*PIECE_SIZE, PIECE_SIZE, PIECE_SIZE))
    def step(self, pos):
        tmp = self.position
        self.position = pos
        return tmp

class Snake:
    head = None
    body = []
    tail = None
    direction = DOWN
    grow = 0
    def __init__(self):
        self.head = SnakeSegment((0,4))
        self.body.append(SnakeSegment((0,3)))
        self.body.append(SnakeSegment((0,2)))
        self.body.append(SnakeSegment((0,1)))
        self.tail = SnakeSegment((0,0))
    def draw(self, surface):
        self.head.draw(surface)
        for piece in self.body:
            piece.draw(surface)
        self.tail.draw(surface)
    def step(self, foods):
        p = self.head.position
        if self.direction == LEFT: p = (p[0]-1, p[1])
        elif self.direction == RIGHT: p = (p[0]+1, p[1])
        elif self.direction == UP: p = (p[0], p[1]-1)
        elif self.direction == DOWN: p = (p[0], p[1]+1)
        
        if p[0] < 0 or p[0] >= GRID_WIDTH or p[1] < 0 or p[1] >= GRID_HEIGHT:
            return
        for piece in self.body:
            if p == piece.position:
                return
        for food in foods:
            if p == food.position:
                snake.grow += 1
                foods.remove(food)
                foods.append(Food())
        
        p = self.head.step(p)
        for piece in self.body:
            p = piece.step(p)
        if snake.grow:
            snake.grow -= 1
            snake.body.append(SnakeSegment(p))
        else:
            self.tail.step(p)

pygame.init()
window = pygame.display.set_mode((GRID_WIDTH*PIECE_SIZE, GRID_HEIGHT*PIECE_SIZE))
pygame.display.set_caption("Snaaaaake")
screen = pygame.display.get_surface()
clock = pygame.time.Clock()
snake = Snake()
foods = []
foods.append(Food())

while True:
    screen.fill((0,0,0))
    for food in foods:
        food.draw(screen)
    snake.draw(screen)
    pygame.display.flip()
    
    time_passed = clock.tick(15)
    for event in pygame.event.get(): 
        if event.type == QUIT:
            sys.exit(0)
        elif event.type == KEYDOWN:
            if event.key == K_LEFT: snake.direction = LEFT
            elif event.key == K_RIGHT: snake.direction = RIGHT
            elif event.key == K_UP: snake.direction = UP
            elif event.key == K_DOWN: snake.direction = DOWN
            elif event.key == K_g: snake.grow += 1
    
    snake.step(foods)

#!/usr/bin/env python
import pygame,os,sys
from pygame.locals import *
import random

pygame.init()
window = pygame.display.set_mode((640, 480))
pygame.display.set_caption("PGPaint")
clock = pygame.time.Clock()
screen = pygame.display.get_surface()
screen.fill((255,255,255))

oldpos = None
while True:
    time_passed = clock.tick(30)
    for event in pygame.event.get():
        if event.type == QUIT:
            sys.exit(0)
        elif event.type == MOUSEMOTION:
            if oldpos and event.buttons[0]:
                pygame.draw.line(screen, (0,0,0), oldpos, event.pos)
            oldpos = event.pos
    pygame.display.flip()
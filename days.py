import pygame,os,sys
from pygame.locals import * 

pygame.init()
window = pygame.display.set_mode((640, 480))
pygame.display.set_caption("Days")
screen = pygame.display.get_surface()
screen.fill((255,255,255))

file = os.path.join("data", "MrBordello_Strategy_Game_Tileset.png")
image = pygame.image.load(file)
screen.blit(image, (0,0))
pygame.display.flip()

while True:
    for event in pygame.event.get(): 
        if event.type == QUIT:
            sys.exit(0)
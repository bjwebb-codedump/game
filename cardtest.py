#!/usr/bin/env python
import pygame,os,sys
from pygame.locals import * 

class Card(pygame.sprite.Sprite):
    position = (0.0, 0.0)
    mouse_pos = (0.0, 0.0)
    def __init__(self, section):
        self.image = section
        self.rect = pygame.Rect(0,0,100,145)
        pygame.sprite.Sprite.__init__(self)
    def moveto(self, p):
        self.position = (p[0]-self.mouse_pos[0], p[1]-self.mouse_pos[1])
        self.rect = pygame.Rect(self.position[0], self.position[1], self.rect.width, self.rect.height)

pygame.init()
window = pygame.display.set_mode((640, 480))
pygame.display.set_caption("Days")
screen = pygame.display.get_surface()
screen.fill((255,255,255))

file = os.path.join("data", "momoko_Deck_of_Playing_Cards.png")
image = pygame.image.load(file)

cards = pygame.sprite.Group()
for y in range(0,4):
    for x in range(0,13):
        cards.add(Card(image.subsurface(pygame.Rect(x*100,y*145,100,145))))

x=0
y=0

dragged = None

while True:
    screen.fill((255,255,255))
    cards.draw(screen)
    pygame.display.flip()
    for event in pygame.event.get(): 
        if event.type == QUIT:
            sys.exit(0)
        elif event.type == MOUSEBUTTONDOWN:
            pos = pygame.mouse.get_pos()
            sprites = cards.sprites()
            sprites.reverse()
            for sprite in sprites:
                if sprite.rect.collidepoint(pos):
                    dragged = sprite
                    sprite.mouse_pos = (pos[0]-sprite.position[0], pos[1]-sprite.position[1])
                    break
        elif event.type == MOUSEBUTTONUP:
            dragged = None
        elif event.type == MOUSEMOTION:
            if dragged:
                dragged.moveto(pygame.mouse.get_pos())

import pygame,os,sys
from pygame.locals import * 

pygame.init()
window = pygame.display.set_mode((640, 480))
pygame.display.set_caption("Test Game")
screen = pygame.display.get_surface()
screen.fill((255,255,255))

blueman_file = os.path.join("data","blueman.png")
blueman_surface = pygame.image.load(blueman_file)
blueman_x = 0
print blueman_x
blueman_y = 0
screen.blit(blueman_surface, (0,0))
pygame.display.flip()

def input(events):
    global blueman_x, blueman_y, screen, blueman_surface
    for event in events:
        if event.type == QUIT:
            sys.exit(0)
        elif event.type == KEYDOWN:
            if event.key == K_LEFT: blueman_x -= 40
            elif event.key == K_RIGHT: blueman_x += 40
            elif event.key == K_UP: blueman_y -= 40
            elif event.key == K_DOWN: blueman_y += 40
            screen.fill((255,255,255))
            screen.blit(blueman_surface, (blueman_x, blueman_y))
            pygame.display.flip()
        else: 
            print event 
 
while True:
    print blueman_x
    input(pygame.event.get())
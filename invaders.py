#!/usr/bin/env python
import pygame,os,sys
from pygame.locals import *
import random

INVADER_WIDTH = 20.0
INVADER_HEIGHT = 20.0
INVADER_SPACING = 10.0

class CollideError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

class vec2:
    def __init__(self, x, y):
        self.v = (x, y)
    def __add__(self, b):
        try:
            return vec2(self.v[0] + b, self.v[1] + b)
        except:
            return vec2(self.v[0] + b.v[0], self.v[1] + b.v[1])
    def __mul__(self, v):
        return vec2(self.v[0]*v, self.v[1]*v)
    def __abs__(self):
        return vec2(abs(self.v[0]), abs(self.v[1]))
    def __getitem__(self, i):
        return self.v[i]
    def __str__(self):
        return "vec2(%f, %f)" % (self.v[0], self.v[1])

LEFT = 0
RIGHT = 1

class Invader(pygame.sprite.Sprite):
    outer_rect = None
    position = vec2(0.0, 0.0)
    velocity = vec2(5.0, 0.0)
    def __init__(self, x, y):
        self.position = vec2(x, y)
        self.image = pygame.Surface((INVADER_WIDTH,INVADER_HEIGHT))
        self.image.fill((255,255,255))
        self.rect = pygame.Rect(self.position[0],self.position[1],INVADER_WIDTH,INVADER_HEIGHT)
        pygame.sprite.Sprite.__init__(self)
        
    def update(self, factor=None, step=1):
        if factor:
            self.velocity = abs(self.velocity) * factor
            self.position += vec2(0, invader_step)
        elif step == 1:
            p = self.position + self.velocity
            r = pygame.Rect(p[0],p[1],self.rect.width,self.rect.height)
            
            if r.right > self.outer_rect.right:
                raise CollideError(-1)
            elif r.left < self.outer_rect.left:
                raise CollideError(1)
            if r.bottom > 440:
                gameover()
        elif step == 2:
            self.position += self.velocity
            self.rect = pygame.Rect(self.position[0],self.position[1],self.rect.width,self.rect.height)

class You(pygame.sprite.Sprite):
    position = vec2(0.0, 440.0)
    velocity = vec2(0.0, 0.0)
    def __init__(self):
        self.image = pygame.Surface((INVADER_WIDTH,INVADER_HEIGHT))
        self.image.fill((0,255,0))
        self.rect = pygame.Rect(self.position[0],self.position[1],INVADER_WIDTH,INVADER_HEIGHT)
        pygame.sprite.Sprite.__init__(self)
    
    def update(self):
        self.position += self.velocity
        self.rect = pygame.Rect(self.position[0],self.position[1],self.rect.width,self.rect.height)

class Bullet(pygame.sprite.Sprite):
    position = vec2(0.0, 440.0)
    velocity = vec2(0.0, -10.0)
    def __init__(self, x, y):
        self.position = vec2(x, y)
        self.image = pygame.Surface((3,INVADER_HEIGHT))
        self.image.fill((255,255,255))
        self.rect = pygame.Rect(self.position[0],self.position[1],3,INVADER_HEIGHT)
        pygame.sprite.Sprite.__init__(self)
    
    def update(self):
        self.position += self.velocity
        self.rect = pygame.Rect(self.position[0],self.position[1],self.rect.width,self.rect.height)


pygame.init()
window = pygame.display.set_mode((640, 480))
pygame.display.set_caption("Invaders")
screen = pygame.display.get_surface()
clock = pygame.time.Clock()
invader_step = 5

invaders = pygame.sprite.Group()
for j in range(0,10):
    for i in range(0,20):
        invader = Invader(i*(INVADER_WIDTH+INVADER_SPACING), j*(INVADER_HEIGHT+INVADER_SPACING))
        invader.outer_rect = screen.get_rect()
        invaders.add(invader)
print len(invaders)

yous = pygame.sprite.Group()
you = You()
yous.add(you)

bullets = pygame.sprite.Group()

while True:
    screen.fill((0,0,0))
    try:
        invaders.update(None, 1)
        invaders.update(None, 2)
    except CollideError as e:
        invaders.update(e.value)
        invaders.update(None, 2)
    invaders.draw(screen)
    
    yous.update()
    yous.draw(screen)
    
    bullets.update()
    bullets.draw(screen)
    pygame.sprite.groupcollide(bullets, invaders, True, True,)
    
    pygame.display.flip()
    
    time_passed = clock.tick(15)
    for event in pygame.event.get(): 
        if event.type == QUIT:
            sys.exit(0)
        elif event.type == KEYDOWN:
            if event.key == K_LEFT: you.velocity = vec2(-10.0, 0.0)
            elif event.key == K_RIGHT: you.velocity = vec2(10.0, 0.0)
            elif event.key == K_SPACE: bullets.add(Bullet(you.position[0], you.position[1]))
        elif event.type == KEYUP:
            if event.key == K_LEFT and you.velocity[0] < 0 \
            or event.key == K_RIGHT and you.velocity[0] > 0:
                you.velocity = vec2(0.0, 0.0)

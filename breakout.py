#!/usr/bin/env python
import pygame,os,sys
from pygame.locals import * 

class Block():
    velocity = (0.0,0.0)
    exists = True
    color = (255, 255, 255)
    rect = None
    def __init__(self, color, rect):
        self.color = color
        self.rect = rect

class Ball():
    velocity = (0.5,-0.5)
    rect = None
    def __init__(self, color, rect):
        self.color = color
        self.rect = rect
    def reverseX(self):
        self.velocity = (self.velocity[0]*-1, self.velocity[1])
    def reverseY(self):
        self.velocity = (self.velocity[0], self.velocity[1]*-1)
    def avoid(self, other_rect):
        if self.rect.bottom > other_rect.top and self.rect.top < other_rect.top \
        or self.rect.top < other_rect.bottom and self.rect.bottom > other_rect.bottom:
            self.reverseY()
        elif self.rect.right > other_rect.left and self.rect.left < other_rect.left \
        or self.rect.left < other_rect.right and self.rect.right > other_rect.right:
            self.reverseX()
    def contain(self, other_rect):
        if self.rect.left < other_rect.left or self.rect.right > other_rect.right:
            self.reverseX()
        elif self.rect.top < other_rect.top:
            self.reverseY()
        elif self.rect.bottom > other_rect.bottom:
            game_over()

def step(v, t):
    return (v*t)/3

pygame.init()
window = pygame.display.set_mode((640, 480))
pygame.display.set_caption("Breakout")
screen = pygame.display.get_surface()
clock = pygame.time.Clock()

blocks = []
line_colors = [(255,0,0), (255,0,0), (255,128,0), (255,128,0), (255,255,0), (255,255,0)]
#line_colors = [(150,0,0), (200,0,0), (255,0,0), (255,50,0), (255,150,0), (255,255,0)]
for j in range(0,6):
    for i in range(0,8):
        blocks.append(Block(line_colors[j], pygame.Rect((i*80)+5, (j*40)+5, 70, 30)))

paddle = Block((0,255,0), pygame.Rect(0,440,100,20))
ball = Ball((255,255,255), pygame.Rect(50,420,20,20))

super_mode = False

while True:
    time_passed = clock.tick(50)

    for event in pygame.event.get(): 
        if event.type == QUIT:
            sys.exit(0)
        elif event.type == KEYDOWN:
            if event.key == K_LEFT: paddle.velocity = (-1.0,0.0)
            elif event.key == K_RIGHT: paddle.velocity = (1.0,0.0)
            elif event.key == K_s: super_mode = not super_mode
        elif event.type == KEYUP:
            if event.key == K_LEFT and paddle.velocity[0] < 0: paddle.velocity = (0.0,0.0)
            elif event.key == K_RIGHT and paddle.velocity[0] > 0: paddle.velocity = (0.0,0.0)
    
    paddle.rect.move_ip(step(paddle.velocity[0], time_passed), step(paddle.velocity[1], time_passed))
    ball.rect.move_ip(step(ball.velocity[0], time_passed), step(ball.velocity[1], time_passed))
    
    screen.fill((0,0,0))
    for block in blocks:
        if block.exists:
            if block.rect.colliderect(ball.rect):
                block.exists = False
                if not super_mode:
                    ball.avoid(block.rect)
            else:
                pygame.draw.rect(screen, block.color, block.rect)
    if paddle.rect.colliderect(ball.rect):
        ball.reverseY()
    if not screen.get_rect().contains(ball.rect):
        ball.contain(screen.get_rect())
        ball.rect.clamp_ip(screen.get_rect())
    pygame.draw.rect(screen, paddle.color, paddle.rect)
    pygame.draw.ellipse(screen, ball.color, ball.rect)
    pygame.display.flip()